<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\EasyLoan;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function store(Request $request) {
        $data = [];
        $data['requirement'] = array(
            "slider" => 36100000,
            "slidersubreuire" => 27,
            "emptype"=> " Salaried",
            "workexp"=> 466,
            "anuincome"=> 466,
            "monthincome"=> 466,
            "emi"=> 46
        );

        $data['personaldetails'] = array(
            "name"=> "Test",
            "gender"=> "Female",
            "date"=> "2022-04-14",
            "marriedstatus"=> " Single",
            "email"=> "harvinder@gmail.com",
            "pannum"=> "57",
            "mobilenum"=> 566566,
            "address"=> "pune",
            "pincode"=> 123456,
            "state"=> "ghfhfh",
            "city"=> "Married",
            "propname"=> "gg",
            "projname"=> "ggg",
            "nameofbuild"=> "ggg",
            "locality"=> "ggg",
            "radio"=> ""
        );

        $data['exploreloan']=array(
            "explorename"=> "fgfg"
        );

        $data['documentverify']=array(
            "file"=> "C=>\\fakepath\\floor2.avif",
            "pancard"=> "C=>\\fakepath\\Visitors (1).xlsx",
            "tancard"=> "C=>\\fakepath\\Visitors (1).xlsx",
            "form"=> "C=>\\fakepath\\Visitors (1).xlsx",
            "itr"=> "C=>\\fakepath\\Visitors (1).xlsx",
            "subpan"=> "C=>\\fakepath\\Visitors (2).xlsx",
            "relation"=> "C=>\\fakepath\\Visitors (1).xlsx",
            "salaryslip"=> "C=>\\fakepath\\Visitors (2).xlsx"
        );
        
        echo "<pre>";
        $json_data = json_encode($data);
        $arr_data = json_decode($json_data, true);
        $actual_data = $query = [];
        foreach($arr_data as $key => $value) {
            foreach($value as $k =>$val) {
                $actual_data[$k] = $val;
            }
        }

        
        // $key = implode(', ', array_keys($actual_data));
        // $value = implode(', :', array_keys($actual_data));
        // echo implode("', '",array_keys($actual_data)); die;
        // $qry = "INSERT INTO easiloan(".$key.") VALUES (:".$value.")";
        // $stmnt = $db->prepare($qry);
        // $stmnt->execute($actual_data);
        // echo "<pre>";
        // print_r($actual_data); die;
        // $status = "error";
        // $msg = "Something went wrong Data not inserted";
        // if($db->lastInsertId() > 0) {
        //     $status = "ok";
        //     $msg = "Data inserted successfully";
        // }

        try {   
            
            //---Insert form data into sql tbl---
            $data = EasyLoan::create($actual_data);  
            $data->save(); 
            //----Insertion end here-------
            $msg = "Data Inserted Successfully";
            $status = "ok";
        } catch (\Throwable $th) {
            $msg = $th->getMessage();
            $status = "error";
        }

        echo json_encode(array('status' => $status, 'msg'=> $msg));
    }
}

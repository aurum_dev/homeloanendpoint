<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EasyLoan extends Model
{
    use HasFactory;
    protected $table='easiloan';
    protected $fillable = ['slider', 'slidersubreuire', 'emptype', 'workexp', 'anuincome', 'monthincome', 'emi', 'name', 'gender', 'date', 'marriedstatus', 'email', 'pannum', 'mobilenum', 'address', 'pincode', 'state', 'city', 'propname', 'projname', 'nameofbuild', 'locality', 'radio', 'explorename', 'file', 'pancard', 'tancard', 'form', 'itr', 'subpan', 'relation', 'salaryslip'];
}
